using System;
using System.Collections.Generic;
using LabWeek5.Models;

public class Subject
{
    private List<IObservable> observers = new List<IObservable>();


    public void Attach(IObservable observer)
    {
        observers.Add(observer);
    }

    public void Detach(IObservable observer)
    {
        observers.Remove(observer);
    }

    public void Notify(string message)
    {
        try {
            foreach (var observer in observers)
            {
                observer.Update(message);
            }
        } catch (Exception err) {
            Console.WriteLine(err.Message);
        }
    }
}