namespace LabWeek5.Models;

public interface IObservable
{
    void Update(string message);
}
