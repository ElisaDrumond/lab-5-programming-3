﻿using System.Diagnostics;
using LabWeek5.Models;

public class Observer : IObservable
{
    public void Update(string message)
    {
        Process process = new();
        process.StartInfo.FileName = "notify-send";
        process.StartInfo.Arguments = $"\"Notification\" \"{message}\"";

        process.Start();
        
        process.WaitForExit();
    }
}
