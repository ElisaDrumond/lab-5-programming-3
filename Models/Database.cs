﻿﻿using System.Collections.ObjectModel;

namespace LabWeek5.Models;

public class Database
{
    private static Database? instance;
    private ObservableCollection<User> users = new ObservableCollection<User>();
    
    public static Database CreateInstance() {
        instance ??= new Database();
        return instance;
    }

    public void SaveUser(User user) {
        Validators(user);
        users.Add(user);
        Observer messageObserver = new();
        Subject subject = new();
        subject.Attach(messageObserver);
        subject.Notify($"User {user.Email} has been added.");
    }

    private void Validators(User user) {
        string errorMessage = "";
        if (!Validator.ValidateEmail(user.Email)) {
            errorMessage = "email";
            InvalidParameterError.ErrorStack.PushError(errorMessage);
        }
        if (!Validator.ValidateName(user.Lastname)) {
            errorMessage = "name";
            InvalidParameterError.ErrorStack.PushError(errorMessage);
        }
        if (!Validator.ValidateName(user.Name)) {
            errorMessage = "lastname";
            InvalidParameterError.ErrorStack.PushError(errorMessage);
        }

        if (!string.IsNullOrEmpty(errorMessage)) throw new InvalidParameterError(errorMessage);
    }

    public ObservableCollection<User> GetUsers() {
        return users;
    }
}