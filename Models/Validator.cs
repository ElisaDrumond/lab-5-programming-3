using System.Text.RegularExpressions;

namespace LabWeek5.Models;

public class Validator
{
    public static bool ValidateEmail(string email) {
        string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";
        Regex regex = new(pattern);

        return regex.IsMatch(email);
    }

    public static bool ValidateName(string name) {
        string pattern = @"^[a-zA-Z0-9_-]{3,16}$";
        Regex regex = new(pattern);

        return regex.IsMatch(name);
    }
}
