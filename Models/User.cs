﻿using System.Text;

namespace LabWeek5.Models;

public class User
{
    public string Email {set; get;}
    public string Lastname {set; get;}
    public string Name {set; get;}


    public User(string name, string email, string lastname) {
        Email = email;
        Lastname = lastname;
        Name = name;
    }

    public string GetToString()
    {
        StringBuilder text = new();

        text.Append($"Email: {Email}");
        text.Append('\n');
        text.Append($"Lastname: {Lastname}");

        return text.ToString();
    }
}
