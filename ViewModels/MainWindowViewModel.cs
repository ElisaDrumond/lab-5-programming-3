﻿﻿using Avalonia.Controls.Notifications;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using LabWeek5.Models;

using LabWeek5.ViewModels;

namespace LabWeek5.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly INotificationManager _notificationManager;

        public ObservableCollection<User> Users { get; set; } = new ObservableCollection<User>();
        public ObservableCollection<User> UsersBeforeUpdate { get; set; } = new ObservableCollection<User>();
        
        public MainWindowViewModel(INotificationManager notificationManager)
        {
            _notificationManager = notificationManager;
        }

        public void AddNewUser(User user)
        {
            Database.CreateInstance().SaveUser(user);
            Users.Add(user);
            _notificationManager.Show(new Notification("Congratulations!", $"You've successfully signed up. Welcome to our community, {user.Name}"));
        }

        public void FilterUser(string? textToFilter)
        {
            string textWithoutSpace = textToFilter?.Trim() ?? "";

            if (string.IsNullOrEmpty(textWithoutSpace))
            {
                // Se o texto de filtro for vazio, restaurar a lista original
                Users.Clear();
                foreach (var user in UsersBeforeUpdate)
                {
                    Users.Add(user);
                }
            }
            else
            {
                // Filtrar e adicionar à lista
                List<User> filteredUsers = UsersBeforeUpdate.Where(obj => obj.Email.Contains(textWithoutSpace)).ToList();
                Users.Clear();
                foreach (var user in filteredUsers)
                {
                    Users.Add(user);
                }
            }
        }

        public bool IsNullOrEmptyText(string? text) => string.IsNullOrEmpty(text);

        public bool CheckIfNotHaveUserToFilter() => Users.Count == 0;
    }
}