using System;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.Interactivity;
using Avalonia.Controls.Notifications;


using LabWeek5.Models;
using LabWeek5.ViewModels;

namespace LabWeek5.Views
{
    public partial class MainWindow : Window
    {
        TextBox nameTextBox;
        TextBox emailTextBox;
        TextBox lastnameTextBox;
        TextBox filterByEmailBox;
        TextBox sucessLabelBox;
        TextBox errorLabelBox;

        private readonly MainWindowViewModel _viewModel;

        public MainWindow()
        {
            InitializeComponent();
            _viewModel = new MainWindowViewModel(new NotificationManager());
            DataContext = _viewModel;
            
            nameTextBox = this.Find<TextBox>("Name")!;
            emailTextBox = this.Find<TextBox>("Email")!;
            lastnameTextBox = this.Find<TextBox>("Lastname")!;
            filterByEmailBox = this.Find<TextBox>("filterByEmail")!;
            sucessLabelBox = this.Find<TextBox>("sucessLabel")!;
            errorLabelBox = this.Find<TextBox>("errorLabel")!;
        }

        public void Handle(string errorMessage, Exception e)
        {
            errorLabelBox.Text = errorMessage;
            errorLabelBox.IsVisible = true;
            sucessLabelBox.IsVisible = false;

            try
            {
                string nameText = nameTextBox.Text!;
                string emailText = emailTextBox.Text!;
                string surnameText = lastnameTextBox.Text!;

                string sucessLabelText = sucessLabelBox.Text!;

                bool existsAllFields = string.IsNullOrEmpty(nameText) || string.IsNullOrEmpty(emailText) || string.IsNullOrEmpty(surnameText);
                if (existsAllFields) return;

                User user = new(nameText!, emailText!, surnameText!);
                _viewModel.AddNewUser(user);

                sucessLabelText = $"Congratulations! You've successfully signed up. Welcome to our community {user.Name}";
                sucessLabelBox.IsVisible = true;
                errorLabelBox.IsVisible = false;

                Clear();
            }
            catch (Exception nameError)
            {
                HandleError(InvalidParameterError.ErrorStack.PopError(), nameError);
            }
        }

        private void HandleError(string errorMessage, Exception exception)
        {
            string errorLabelText = errorLabelBox.Text!;
            errorLabelText = errorMessage;
            errorLabelBox.IsVisible = true;
            sucessLabelBox.IsVisible = false;
        }

        private void FilterTextUpdate(object sender, KeyEventArgs e)
        {
            string filterByEmailTextBox = filterByEmailBox.Text!;
            _viewModel.FilterUser(filterByEmailTextBox);
        }

        private void Clear()
        {
            nameTextBox.Text = "";
            emailTextBox.Text = "";
            lastnameTextBox.Text = "";
        }
    }
}
